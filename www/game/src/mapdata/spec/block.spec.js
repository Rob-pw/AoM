import test from 'tape'

import
 { create_block_fromMapLayout
 , create_block }
from '../block'

import
 { loadMission }
from '~/mission/load'

import
 { definition }
from '~/mission/missions/0.prologue'

import
 { fill_levelState }
from '~/mission/state'

const
  { items }
 = definition
;

const
  { mapLayout }
 = { items } :: loadMission('prologue')
;

test
 ( 'Can create a block from a map layout'
 , t =>
  { console.log(mapLayout)
    const
      layers
     = { static: null
       , dynamic: null
       , abstract: null }
  ; for(const
      layerType in layers)
    { const
        block
       = create_block_fromMapLayout
        ( mapLayout)
    ; layers[layerType]
        = block }

    const
      filled_state
     = fill_levelState
      ( layers)
      ( mapLayout)
  ;
    //console.log(block)
    console.log(JSON.stringify(filled_state))})
