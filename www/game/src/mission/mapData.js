const
  { slice }
 = Array.prototype
;

export const
  plan
 = (chars) => chars.join('').trim()
;

const
  getRawRows
 = rawLayout =>
  rawLayout.split('\n')
,
  getDataItem
 = itemsMap => rawDataItem =>
  itemsMap[rawDataItem]
,
  getRawRowData
 = dataItemGetter => rawRow =>
  slice
  . apply(rawRow)
  . map(dataItemGetter)
,
  getMapLayout
 = (itemsMap, plan) =>
  getRawRows(plan.trim())
  . map(getRawRowData(getDataItem(itemsMap)))
;

export { getMapLayout }
