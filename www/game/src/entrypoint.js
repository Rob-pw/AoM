import path from 'path'

import
 { loadMission }
from '~/mission/load'

import
 { start }
from '~/logic/game.run'


//import '~/physics/'
//import '~/.cruft/overrides'

/* game.player[0]
 . walk(right)
 . walk(right)
 . jump(right) */
//
// function
//  spawn()
// { console.log(this) }
//
// const
//   player
//  = { move(coords)
//     { console.log(this) } }
// ;
//
// function
//  instructions()
// { this::spawn(player)
// ; this::player.move(coords) }
//
// instructions.call(game)
