"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMapLayout = void 0;
const {
  slice
} = Array.prototype;

const getRawRows = rawLayout => rawLayout.split('\n'),
      getDataItem = item_map => rawDataItem => item_map[rawDataItem],
      getRawRowData = dataItemGetter => rawRow => slice.apply(rawRow).map(dataItemGetter),
      getMapLayout = (item_map, level) => getRawRows(level.plan.trim()).map(getRawRowData(getDataItem(item_map)));

exports.getMapLayout = getMapLayout;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMapLayout = void 0;
const {
  slice
} = Array.prototype;

const getRawRows = rawLayout => rawLayout.split('\n'),
      getDataItem = item_map => rawDataItem => item_map[rawDataItem],
      getRawRowData = dataItemGetter => rawRow => slice.apply(rawRow).map(dataItemGetter),
      getMapLayout = (item_map, level) => getRawRows(level.plan.trim()).map(getRawRowData(getDataItem(item_map)));

exports.getMapLayout = getMapLayout;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.plan = void 0;
const plan = `
......................
..#................#..
..#..............=.#..
..#.........o.o....#..
..#.@......#####...#..
..#####............#..
......#++++++++++++#..
......##############..
......................`;
exports.plan = plan;
"use strict";

var _Level = require("./Level");

var level0 = _interopRequireWildcard(require("./levels/0.prologue"));

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function () {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
    return {
      default: obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj.default = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

const air = Symbol('air'),
      wall = Symbol('wall'),
      coin = Symbol('coin'),
      spawn = Symbol('spawn'),
      exit = Symbol('exit'),
      lava = Symbol('lava');
const item_map = {
  '.': air,
  'o': coin,
  '#': wall,
  '@': spawn,
  '=': exit,
  '+': lava
};
const mapLayout = (0, _Level.getMapLayout)(item_map, level0);
console.log(mapLayout);
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.plan = void 0;
const plan = `
......................
..#................#..
..#..............=.#..
..#.........o.o....#..
..#.@......#####...#..
..#####............#..
......#++++++++++++#..
......##############..
......................`;
exports.plan = plan;
"use strict";

var _Level = require("~/Level");

var level0 = _interopRequireWildcard(require("~/levels/0.prologue"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const air = Symbol('air'),
      wall = Symbol('wall'),
      coin = Symbol('coin'),
      spawn = Symbol('spawn'),
      exit = Symbol('exit'),
      lava = Symbol('lava');
const item_map = {
  '.': air,
  'o': coin,
  '#': wall,
  '@': spawn,
  '=': exit,
  '+': lava
};
const mapLayout = (0, _Level.getMapLayout)(item_map, level0);
console.log(mapLayout);
