import
 * as base
from './base'

import
 * as prologue_imports
from './0.prologue'

export const
  prologue
 = { items: base.items
   , ...prologue_imports }
;
