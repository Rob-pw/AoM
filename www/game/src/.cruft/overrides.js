import
 { $ }
from '~/.cruft/utils'

const
  logger
 = log => function
 logOverride( toLog)
{ const
    logElem
   = $('x-cvs-log#mainLog > pre')
;
  logElem
   . innerText +=
      typeof toLog === 'string'
       ? toLog + '\r\n'
       : ""
;
  log(toLog) }

let log = ::console.log
console.log = logger(log)
