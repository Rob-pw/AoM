const
  assign
 = root => value =>
  value
   ? (root.shift()
     , root.unshift(value))
   : root[0]
;

const
  set
 = assign => (host) => (prop, value) =>
  Reflect.set(host, prop, value)
;

let root = []
const
  attach
 = root => (prop) =>
  function(value)
  { const
      host
     = this
  ;
    set(assign(root))
     ( host)
     ( prop, value)

    return host }
, primitive
 = function( value)
  { const
      host
     = this
    , hintFn
     = value => hint => value
  ; return attach(root)(Symbol.toPrimitive)
      . call(host, hintFn(value)) }
;

export const
  associate
 = { primitive }
;
