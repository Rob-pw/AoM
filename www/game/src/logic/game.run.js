const
  requestAnimationFrame
 = fn => setTimeout(fn, 0)

const
  gameFrame
 = game => state =>
  game(state)
;

const
  next
 = (fn, state) =>
  requestAnimationFrame
   ( () =>
    { const
        result
       = fn(state)
    ; console.log('next', result)
      if(result != false)
        next(fn, result)
      else console.log('end') })
;

export const
  start
 = game => function(mission)
  { const
      base_state
     = this
  ;
    next
   ( gameFrame(game(mission))
   , mission.getInitialState(base_state)) }
;

let count = 3
start(mission => state =>
  (console.log(mission, state)), count--)
  ({ getInitialState: () => ({ foo: 0 }) })
