export const
  Door
 = (isOpen) =>
  ({ locked: !isOpen })
;
