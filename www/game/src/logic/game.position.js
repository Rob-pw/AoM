const
  getPosition
 = mapLayout => (x, y) =>
  mapLayout[x][y]
;

const
  getBelow
 = (x, y) => getPosition(x, y - 1)
, getAbove
 = (x, y) => getPosition(x, y + 1)
, getLeft
 = (x, y) => getPosition(x - 1, y)
, getRight
 = (x, y) => getPosition(x + 1, y)
;

const
  symbols
 = { left: Symbol('move left')
   , right: Symbol('move right')
   , above: Symbol('move up')
   , below: Symbol('move down') }
;
