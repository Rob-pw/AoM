function
 getLargestRow(highest, current)
{ const
    currentRows
   = current.length
; return highest > currentRows
    ? highest 
    : currentRows }

const
  getBlockSize
 = rows =>
  ({ y: rows.length
   , x: rows
        . reduce
         ( getLargestRow
         , 0) })
,
  create_block
 = (x_max, y_max) =>
  { const
      state = []
  ;
    for(let y = 0
      ; y < y_max
      ; y += 1)
    { if(!state[y])
        state[y] = Array(x_max) }

  ; return state }
;

export const
  create_block_fromMapLayout
 = (mapPlan) =>
  { const
      { x, y }
     = getBlockSize(mapPlan)
  ;
    return create_block( x, y) }
;
