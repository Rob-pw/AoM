import
 { getMapLayout }
from './mapData'

import
 * as missions
from '~/mission/missions'

export function
 loadMission(levelName)
{ const
    { items }
   = this
  , { mapPlan, ...props }
   = missions[levelName].definition
;
  const
    mapLayout
   = getMapLayout
    ( items, mapPlan)
;
  return (
    { mapLayout
    , ...props }) }

export const
  appendState
 = state => newState =>
  Object.assign
   ( {}
   , state
   , newState)
;
