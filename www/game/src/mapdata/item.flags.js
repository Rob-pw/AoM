export const
  flags =
   { player: Symbol('represents the player')
   , solid: Symbol('object is solid, cannot be walked through')
   , collides: Symbol('has a special effect when collided with')
   , takesDamage: Symbol('item has a health bar and may be killable')
   , static: Symbol('will never be modified once instantiated')
   , unique: Symbol('will never be present more than once')
   , active: Symbol('is actively listening for state changes')
   , abstract: Symbol('is not an actual map element')
   , ignore: Symbol('placeholder, do not process') }
;
