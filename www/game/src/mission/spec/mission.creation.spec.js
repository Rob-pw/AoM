import test from 'tape'

import
  { loadMission }
from '../load'

import
 { createMission }
from '../create'

import
 * as prologue
from '~/mission/missions/0.prologue'

const
  { items, mapPlan
  , objectDefinitions
  , getInitialState }
 = prologue.definition
;

console.log(mapPlan)

function
  test_mission0(t)
{ createMission
; t.ok(true)
; t.end() }

test
 ( 'Create mission0'
 , test_mission0)
