import { plan } from '~/mapdata/Level'

import { Barrel } from '~/models/Barrel'
import { Key } from '~/models/Key'
import { Door } from '~/models/Door'

import
 { types }
from '~/mapdata/sets/0.basic'

export { items }
from '~/mapdata/sets/0.basic'

const
  door0
 = Door()
,
  key0
 = Key(door0)
,
  barrel0
 = Barrel(key0)
;

export const
  mapPlan
 = plan`
 xxxxx
 x a x
xx   xxxxx xxxxx
x    &xxx  x a xxx
x@    |?x  x    &x
x     xxxxxx   xxxx xxxxx
xx x xx?  x <    > x x ?x
  x*x xx xxxxxxxxxx x   x
 x   x  x& ?x ^   x     &x
 x    x  x  |         xxxx
 x        xxxxxxx   xxx
 xx        <      x
  xxxxxxxxxxxxxxxx
  `
;

export const
  objectDefinitions
 = { [types.barrel]: [ barrel0]
   , [types.door]: [ door0] }
;

export const
  getInitialState
 = base => base.append
  ({ easteregg: 'demo!' })
;
