import
 { planck }
from 'planck-js/dist/planck-with-testbed.commonjs'

const
  { testbed
  , World }
 = planck
;

function
 tick()
{}

function
 demo(tb)
{ tb.width = 300
; tb.height = 200
; tb.step = tick
; tb.ratio = 64
; tb.y = 0 }

testbed
 ( 'Demo'
 , demo )
