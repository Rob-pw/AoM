import
 { associate }
from '~/.cruft/associate'

import
 { flags }
from '../item.flags'

export const
 types =
  { air: Symbol('air')
  , wall: Symbol('wall')
  , coin: Symbol('coin')
  , spawn: Symbol('entity spawn')
  , exit: Symbol('exit')
  , barrel: Symbol('barrel')
  , door: Symbol('door')
  , mystery: Symbol('mystery reward')
  , wall_safe: Symbol('hidden safe')
  , floor_safe: Symbol('hidden safe')
  , left_jump: Symbol('(left) jump now')
  , right_jump: Symbol('(right) foot lets jump')
  , upward_jump: Symbol('(up) two hops two hops') }
;

const
  make_flagsMap
 = (flags) =>
  flags.reduce
   ( (acc, cur) =>
    ( acc.set(cur, true)
    , acc)
   , new Map())
;

const
  make_definition
 = function(...flags)
  { const
      type
     = this
    , definition
     = { type, flags: make_flagsMap(flags) }
  ;
    return definition :: associate.primitive(type) }
;

export const
  definitions =
 { wall: types.wall :: make_definition(flags.solid, flags.static)
 , spawn: types.spawn :: make_definition(flags.static, flags.abstract, flags.unique)
 , exit: types.exit :: make_definition(flags.collides, flags.static)
 , coin: types.coin :: make_definition(flags.collides)
 , barrel: types.barrel :: make_definition(flags.solid, flags.takesDamage)
 , door: types.door :: make_definition(flags.solid, flags.active)
 , mystery: types.mystery :: make_definition(flags.collides)
 , air: types.air :: make_definition(flags.ignore)
 , left_jump: types.left_jump :: make_definition(flags.collides)
 , right_jump: types.right_jump :: make_definition(flags.collides)
 , upward_jump: types.upward_jump :: make_definition(flags.collides)
 , wall_safe: types.wall_safe :: make_definition(flags.solid, flags.active)
 , floor_safe: types.floor_safe :: make_definition(flags.collides) };

export const
  items
 = { ' ': definitions.air, 'o': definitions.coin
   , 'x': definitions.wall, '@': definitions.spawn
   , '*': definitions.exit, '&': definitions.barrel
   , '|': definitions.door, '?': definitions.mystery
   , '^': definitions.upward_jump
   , '>': definitions.right_jump, '<': definitions.left_jump }
;
