

export const
  preparePlacements
 = game =>
  ({ [spawn]
    : spawn => game.spawn = spawn
   , [barrel]
    : barrel => game.barrel.push(barrel)
   , [door]
    : door => game.door.push(door)
   , [exit]
    : exit => game.exit = exit
   , [mystery]
    : mystery => game.rewards.push(mystery) })
;
