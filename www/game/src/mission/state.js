import
 { flags }
from '~/mapdata/item.flags'

const
  make_itemPlacement
 = (item) => (location) =>
  ({ location, item })

function
 place(item, [ x, y])
{ const
    state
   = this
  , itemFlags
   = item.flags
;
  if(itemFlags.has(flags.ignore))
    return;

  let store;
  if(itemFlags.has(flags.abstract))
    store = state.abstract
  else if(itemFlags.has(flags.static))
    store = state.static
  else
    store = state.dynamic
; console.log(store)
  store[x][y] = item
  return store; }

export const
  fill_levelState
 = initialState => mapLayout =>
  { for(let y = 0
      ; y < mapLayout.length
      ; y += 1)
    { const
        row
       = mapLayout[y]
    ;
      for(let x = 0
        ; x < row.length
        ; x += 1)
      { const
          item
         = mapLayout[y][x]
      ;
        if(!item) continue

        initialState :: place(item, [x, y]) } }
  ;
    return initialState }
